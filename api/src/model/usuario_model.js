const mysqlConnection = require('../common/database_conn');
var bcrypt = require('bcrypt');

var BCRYPT_SALT_ROUNDS = 12;

// Login usuario: valida contrasenia y regresa informacion del usuario
exports.loginUsuario = function (user,result) {
    mysqlConnection.query('SELECT * FROM user WHERE email = ?',[user.email],(err,rows) =>{
        if (!err){
            if(rows.length > 0){
                bcrypt.compare(user.password, rows[0].password, (err, res) => {
                    res ? result(null, "Authorized"): result(null, "Unauthorized"); 
                });
            }else{
                result(err, "Not found");
            }
        }else{
            result(err, rows);
        }
    });
};

// Inserta un nuevo usuario
exports.insertaUsuario = function (user,result) { 
    mysqlConnection.query('SELECT * FROM user WHERE email = ?',[user.email],(err,rows) =>{
        if (!err){
            if(rows.length > 0){
                result(null, "Exist");
            }else{
                bcrypt.hash(user.password, BCRYPT_SALT_ROUNDS, (err, hash) => {
                    mysqlConnection.query('INSERT INTO user (password, email) VALUES (?,?)',[hash,user.email],(err,rows) =>{
                        if(!err){
                            result(null, rows);
                        } else{
                            result(err, rows);
                        }
                    });
                }); 
            }
        }else{
            result(err, rows);
        }
    });  
};

