require('dotenv').config();
const mysql = require('mysql');

const mysqlConnection = {
    host: 'DontShareMySecret',
    user: 'DontShareMySecret',
    password: 'DontShareMySecret',
    database: 'DontShareMySecret'
}

var connection;

function handleDisconnect(){
    connection = mysql.createConnection(mysqlConnection);

    connection.connect(function(err){
        if(err){
            console.log(err);
            setTimeout(handleDisconnect, 2000);
        }else{
            console.log('La base de datos esta conectada');
        }
    });

    connection.on('error', function(err) {
        console.log('db error', err);
        if(err.code === 'PROTOCOL_CONNECTION_LOST') { 
            handleDisconnect();                         
        } else {                                      
            throw err;                                  
        }
    });
}

handleDisconnect();

module.exports = connection;
