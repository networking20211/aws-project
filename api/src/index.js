const express = require('express');
const app = express();
const cors = require('cors');


// Configuraciones
app.set('port', 3000);

// Middlewares
app.use(express.json());

// Use it before all route definitions
app.use(cors({origin: 'http://localhost:3000'}));

// Rutas
app.use(require('./routes/usuario_route.js'));

// Servidor activo
app.listen(app.get('port'), () => {
    console.log('Server on port', app.get('port'));
});
