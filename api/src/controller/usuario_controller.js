const usuario_model = require('../model/usuario_model');
const { response } = require('express');


exports.insertaUsuarioC = function(req, res) {
    const userI = req.body;
    usuario_model.insertaUsuario(userI,function(err,response) {
      if (!err){
        if(response == "Exist") res.status(400).json({result: 0, message: 'Este email ya está registrado, intenta con uno distinto'})
        else res.status(200).json({result: 1, message: 'Usuario agregado correctamente'})
      } else{
        res.status(500).json({result: 0, message: err})
      }
    });
};

exports.loginUsuarioC = function(req, res) {
  const user = req.body;
  usuario_model.loginUsuario(user,function(err,response) {
    if(response == "Not found") res.status(404).json({result: 0, message: 'No existe un usuario registrado con ese email'})
    else if(response == "Authorized") res.status(200).json({result: 1, message: 'El usuario ingresó correctamente'})
    else if(response == "Unauthorized") res.status(401).json({result: 0, message: 'Contraseña incorrecta, verifícala e intenta de nuevo'})
    else res.status(500).json({result: 0, message: 'Hubo un error en el servidor, inténtalo más tarde'})
  });
};