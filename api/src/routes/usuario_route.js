const express = require('express');

const router = express.Router();
const usuario_controller = require('../controller/usuario_controller');

// Login de un usuario
router.post('/api/login/',usuario_controller.loginUsuarioC);

// Inserta un nuevo usuario
router.post('/api/registra/',usuario_controller.insertaUsuarioC);

module.exports = router;