
resource "aws_security_group" "sg_front_server" {

  name        = "front server sg"
  description = "Allow http and https inbound traffic"
  vpc_id      = module.vpc.vpc_id

  ingress {
    from_port   = 8
    to_port     = 0
    protocol    = "icmp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port        = 128
    to_port          = 0
    protocol         = "icmpv6"
    ipv6_cidr_blocks = ["::/0"]
  }

  ingress {
    description = "allow HTTP"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "allow HTTPS"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description      = "allow HTTP from ipv6"
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    ipv6_cidr_blocks = ["::/0"]
  }

  ingress {
    description      = "allow HTTPS from ipv6"
    from_port        = 443
    to_port          = 443
    protocol         = "tcp"
    ipv6_cidr_blocks = ["::/0"]
  }

  ingress {
    description     = "allow SSH"
    from_port       = 22
    to_port         = 22
    protocol        = "tcp"
    security_groups = [aws_security_group.sg_bastion_host.id]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    ipv6_cidr_blocks = ["::/0"]
  }
}


resource "aws_instance" "front_server" {
  ami                    = "ami-0dba2cb6798deb6d8"
  instance_type          = "t2.micro"
  key_name               = var.key_name
  vpc_security_group_ids = [aws_security_group.sg_front_server.id]
  subnet_id              = module.vpc.public_subnets[0]
  ipv6_address_count     = 1
  user_data              = <<EOF
  #! /bin/bash
  sudo apt update
  sudo snap install core; sudo snap refresh core
  sudo snap install --classic certbot
  sudo ln -s /snap/bin/certbot /usr/bin/certbot
EOF

  tags = {
    Name = "Front Server"
  }
}

resource "aws_eip" "front_elastic_ip" {
  instance = aws_instance.front_server.id
  vpc      = true
}


output "front_server_private_ip" {
  value = aws_instance.front_server.private_ip
}

output "front_server_elastic_ip" {
  value = aws_eip.front_elastic_ip.public_ip
}

output "front_server_ipv6" {
  # value = aws_eip.front_elastic_ip.public_ip
  value = [aws_instance.front_server.ipv6_addresses]
}
