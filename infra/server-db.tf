# mysql security group
resource "aws_security_group" "sg_data_server" {

  name        = "data server sg"
  description = "Allow mysql inbound traffic"
  vpc_id      = module.vpc.vpc_id

  ingress {
    description     = "allow TCP"
    from_port       = 3306
    to_port         = 3306
    protocol        = "tcp"
    security_groups = [aws_security_group.sg_back_server.id]
  }

  ingress {
    description     = "allow SSH"
    from_port       = 22
    to_port         = 22
    protocol        = "tcp"
    security_groups = [aws_security_group.sg_bastion_host.id]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

# mysql ec2 instance
resource "aws_instance" "data_server" {
  ami                    = "ami-0dba2cb6798deb6d8"
  instance_type          = "t2.micro"
  key_name               = var.key_name
  vpc_security_group_ids = [aws_security_group.sg_data_server.id]
  subnet_id              = module.vpc.private_subnets[0]
  user_data              = file(abspath("./scripts/start-db.sh"))
  tags = {
    Name = "Data Server"
  }
}

output "data_server_ip" {
  value = aws_instance.data_server.private_ip
}
