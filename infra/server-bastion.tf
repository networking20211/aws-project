resource "aws_security_group" "sg_bastion_host" {
  name        = "sg bastion host"
  description = "bastion host security group"
  vpc_id      = module.vpc.vpc_id

  ingress {
    description = "allow SSH"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}


resource "aws_instance" "bastion_host" {
  depends_on = [
    aws_security_group.sg_bastion_host,
  ]
  ami                    = "ami-0dba2cb6798deb6d8"
  instance_type          = "t2.micro"
  key_name               = var.key_name
  vpc_security_group_ids = [aws_security_group.sg_bastion_host.id]
  subnet_id              = module.vpc.public_subnets[0]
  user_data              = <<EOF
#! /bin/bash
sudo apt update
sudo apt-get install fail2ban emacs -y
EOF
  tags = {
    Name = "bastion host"
  }

  provisioner "file" {
    source      = "${var.base_path}${var.key_name}.pem"
    destination = "/${var.key_name}.pem"

    connection {
      type        = "ssh"
      user        = "ubuntu"
      private_key = tls_private_key.private_key.private_key_pem
      host        = aws_instance.bastion_host.public_ip
    }
  }
}

output "bastion_server_ip" {
  value = aws_instance.bastion_host.public_ip
}
