resource "aws_route53_zone" "private" {
  name = "anothernetworkingproject.tk."

  vpc {
    vpc_id = module.vpc.vpc_id
  }
}


resource "aws_route53_record" "db_dns" {
  zone_id = aws_route53_zone.private.zone_id
  name    = "db.anothernetworkingproject.tk"
  type    = "A"
  ttl     = "300"
  records = [aws_instance.data_server.private_ip]
}

resource "aws_route53_record" "front_cname" {
  zone_id = aws_route53_zone.private.zone_id
  name    = "www.anothernetworkingproject.tk"
  type    = "CNAME"
  ttl     = "300"
  records = ["web.anothernetworkingproject.tk"]
}

resource "aws_route53_record" "front_dns" {
  zone_id = aws_route53_zone.private.zone_id
  name    = "web.anothernetworkingproject.tk"
  type    = "A"
  ttl     = "300"
  records = [aws_instance.front_server.private_ip]
}

resource "aws_route53_record" "api_dns" {
  zone_id = aws_route53_zone.private.zone_id
  name    = "api.anothernetworkingproject.tk"
  type    = "A"
  ttl     = "300"
  records = [aws_instance.back_server.private_ip]
}

resource "aws_route53_record" "email_dns" {
  zone_id = aws_route53_zone.private.zone_id
  name    = "mail.anothernetworkingproject.tk"
  type    = "A"
  ttl     = "300"
  records = [aws_instance.email_server.private_ip]
}


resource "aws_route53_record" "email_mx_dns" {
  zone_id = aws_route53_zone.private.zone_id
  name    = "mail.anothernetworkingproject.tk"
  type    = "MX"
  ttl     = "300"
  records = ["10 mail.anothernetworkingproject.tk"]
}
