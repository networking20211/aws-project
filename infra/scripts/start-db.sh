#! /bin/bash
sudo apt update
sudo apt-get install docker.io -y
sudo systemctl restart docker
sudo systemctl enable docker
sudo groupadd docker
sudo usermod -aG docker $USER
newgrp docker
sudo docker pull mysql
sudo docker run --name mysql -e MYSQL_ROOT_PASSWORD=root \
-e MYSQL_DATABASE=wordpressdb -p 3306:3306 -d mysql:5.7
