snakeApp.controller("ingresarCtrl", function($rootScope, snakeService, $window){
  $rootScope.dataLogin = {};
  $rootScope.dataLogin.email = "";
  $rootScope.dataLogin.password = "";

  function validaEmail(email){
      const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return re.test(String(email).toLowerCase());
  }

  $rootScope.entrar = function(formIngresar){
      if(validaEmail($rootScope.dataLogin.email)){
        if($rootScope.dataLogin.password.length >= 4){
          var urlIngresar = "https://anothernetworkingproject.tk/api/login";
          
          var promesa = snakeService.ingresar(urlIngresar, "POST", JSON.stringify($rootScope.dataLogin))
          .then(function(result){
            if(result.result == 1){
              $window.location.href = "/snake.html";
            }else{
              alert(result.message);
              $rootScope.dataLogin.email = "";
              $rootScope.dataLogin.password = "";
            }
          }).catch(e => {
            if(e.status == 404){
              alert('No existe un usuario registrado con ese email');
            }else if(e.status == 401){
              alert('Contraseña incorrecta, verifícala e intenta de nuevo');
            }else{
              alert('Hubo un error en el servidor, inténtalo más tarde');
            }
          })
      }else{
        alert("Verifica tu contraseña");
      }
    }else{
      alert("Verifica tu correo");
    }
  }
});