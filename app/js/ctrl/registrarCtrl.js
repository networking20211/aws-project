snakeApp.controller("registrarCtrl", function($rootScope, snakeService, $window){
$rootScope.dataLogin = {};
$rootScope.dataLogin.email = "";
$rootScope.dataLogin.password = "";


    function validaEmail(email){
        const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }

    $rootScope.entrar = function(formRegistrar){
        if(validaEmail($rootScope.dataLogin.email)){
            if($rootScope.dataLogin.password.length >= 4){
                var urlIngresar = "https://anothernetworkingproject.tk/api/registra";   
                var promesa = snakeService.ingresar(urlIngresar, "POST", JSON.stringify($rootScope.dataLogin))
                .then(function(result){
                    if(result.result == 1){
                        $window.location = "/";
                    }else{
                        $rootScope.dataLogin.email = "";
                        $rootScope.dataLogin.password = "";
                    }    
                }).catch(e => {
                    console.log(e.status);
                    if(e.status == 400){
                        alert("Este correo ya está registrado, intenta con uno distinto")
                    }else{
                        alert("Error en el servidor, intente más tarde")
                    }
                })
            }else{
                alert("Escribe al menos 4 caracteres");
            }
        }else{
            alert("Escribe un correo válido");
        }
    }
});